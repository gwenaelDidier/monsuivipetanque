import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import { AngularFireDatabase  } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the PetanquePage page.
 *
 * Ionic pages and navigation.
 */

@Component({
  selector: 'component-qualif',
  templateUrl: 'qualif.html'
})
export class QualifComponent {

  public myChampionnat:Observable<any[]>;
  public displayFormQualif: boolean = false;
  public qualifForm: any;
  public nbQualifs: number;
  public annee:number;
  public uid:string;

  private urlChampionnat: string = '';

  constructor(
    private _storage: Storage,
    private db: AngularFireDatabase) {

  }

  public ngOnInit(): void {

    this.annee = new Date().getFullYear();
    let that = this;
    this._storage.get('user').then((val) => {
      that.uid = val.uid;
      that.urlChampionnat = '/' + that.uid + '/championnat';
      that.myChampionnat = that.db.list(that.urlChampionnat).valueChanges();
      that.getNbChampionnats(that.annee);
    });

    // init form
    this.initForm();
  }

  private initForm(): void {
    this.qualifForm = {
        id: null,
        niveau: 'secteur',
        nbEquipes: 0,
        formation: '',
        estQualifie: false
    };
  }


  private getNbChampionnats(year: number): void {
    this.nbQualifs = 0;
    this.myChampionnat.forEach(element => {
      element.forEach(championnat => {
        if(championnat.annee === year){
          this.nbQualifs++;
        }
      });
    });
  }

  public onChangeYear($event): void {
    this.getNbChampionnats($event);
  }

  public save(): void {

    if(this.qualifForm.id && this.qualifForm.id !== null && this.qualifForm.id !== ''){
      this.db.list(this.urlChampionnat).update(this.qualifForm.id, this.qualifForm);
    } else {
      this.db.list(this.urlChampionnat).push(this.qualifForm);
    }

    this.displayFormQualif = false;
  }

  public editConcours(concours: any): void {
    /*this.qualifForm.controls['id'].setValue(concours.$key);
    this.qualifForm.controls['ville'].setValue(concours.ville);
    this.qualifForm.controls['club'].setValue(concours.name);
    this.qualifForm.controls['date'].setValue(concours.date);
    this.qualifForm.controls['formation'].setValue(concours.formation);
    this.qualifForm.controls['nbEquipes'].setValue(concours.nbEquipes);
    this.qualifForm.controls['nbPartiesGagnees'].setValue(concours.nbPartiesGagnees);
    this.qualifForm.controls['nbPartiesPerdues'].setValue(concours.nbPartiesPerdues);
    this.qualifForm.controls['resultat'].setValue(concours.resultat);
    this.qualifForm.controls['repechage'].setValue(concours.repechage);
    this.qualifForm.controls['poste'].setValue(concours.poste);
    this.qualifForm.controls['points'].setValue(concours.points);

    this.displayFormQualif = true;*/
  }

  public generateFormation(formation: string): void {
    console.log(formation);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PetanquePage');
  }

}
