import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {Storage} from '@ionic/storage';


import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/**
 * Generated class for the PetanquePage page.
 *
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-petanque',
    templateUrl: 'petanque.html'
})
export class PetanquePage {

    public myConcoursRef: AngularFireList<any>;
    public myConcours: Observable<any[]>;
    public myChampionnats: Observable<any[]>;
    public displayFormConcours: boolean = false;
    public displayFormQualifs: boolean = false;
    public nbConcours: number;
    public nbChampionnats: number;
    public annee: number;
    public uid: string;
    public typeCompetition: string;

    private urlConcours: string = '';
    private urlChampionnats: string = '';

    constructor(private _storage: Storage,
                private db: AngularFireDatabase) {

    }

    public ngOnInit(): void {

        this.nbConcours = 0;
        this.typeCompetition = TYPE_CONCOURS;
        this.annee = new Date().getFullYear();
        let that = this;
        this._storage.get('user').then((val) => {
            that.uid = val.uid;
            that.urlConcours = '/' + that.uid + '/concours';
            that.myConcoursRef = that.db.list(that.urlConcours);

            that.myConcours = that.myConcoursRef.valueChanges();

            /*that.myConcours = that.myConcoursRef.snapshotChanges().map(changes => {
                return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
            });*/

            that.urlChampionnats = '/' + that.uid + '/championnats';
            that.myChampionnats = that.db.list(that.urlChampionnats).valueChanges();

            that.getNbConcours(that.annee);
            that.getNbChampionnat(that.annee);
        });

        // init form
        this.initForm();
    }

    private initForm(): void {

    }

    public addConcours(): void {
        this.displayFormConcours = true;
    }

    public addQualif(): void {
        this.displayFormQualifs = true;
    }

    public cancelConcours(): void {
        this.displayFormConcours = false;
    }

    public cancelQualif(): void {
        this.displayFormQualifs = false;
    }

    private getNbConcours(year: number): void {
        this.nbConcours = 0;
        this.myConcours.forEach(element => {
            element.forEach(concours => {
                if (concours.annee === year) {
                    this.nbConcours++;
                }
            });
        });
    }

    private getNbChampionnat(year: number): void {
        this.nbChampionnats = 0;
        this.myChampionnats.forEach(element => {
            element.forEach(concours => {
                if (concours.annee === year) {
                    this.nbChampionnats++;
                }
            });
        });
    }

    public onChangeYear($event): void {
        this.getNbConcours($event);
        this.getNbChampionnat($event);
    }

    public generateFormation(formation: string): void {
        console.log(formation);
    }

    public selectConcours(): void {
        console.log('concours');
    }

    public selectChampionnat(): void {
        console.log('championnat');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PetanquePage');
    }

}

export const TYPE_CONCOURS: string = 'concours';
export const TYPE_CHAMPIONNAT: string = 'championnat';
