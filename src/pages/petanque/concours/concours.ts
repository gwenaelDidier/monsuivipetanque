import {Component, Input} from '@angular/core';
import { ConcoursService } from './concours.service';

@Component({
    selector: 'component-concours',
    templateUrl: 'concours.html'
})
export class ConcoursComponent {

    @Input('concours')
    public concours: any;

    @Input('annee')
    public annee: number;

    constructor(private _concoursService: ConcoursService){}

    public editConcours(concours: any) {
        this._concoursService.editConcours(concours);
    }
}
