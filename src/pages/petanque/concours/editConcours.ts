import {Component} from '@angular/core';
import {Storage} from '@ionic/storage';

import {AngularFireDatabase} from 'angularfire2/database';

import {Observable} from 'rxjs/Observable';

/**
 * Generated class for the PetanquePage page.
 *
 * Ionic pages and navigation.
 */

@Component({
    selector: 'component-edit-concours',
    templateUrl: 'editConcours.html'
})
export class EditConcoursComponent {

    public myConcours: Observable<any[]>;
    public displayFormConcours: boolean = false;
    public concoursForm: any;
    public nbConcours: number;
    public annee: number;
    public uid: string;
    public partenaire1: string;
    public partenaire2: string;

    private urlConcours: string = '';

    constructor(private _storage: Storage,
                private db: AngularFireDatabase) {

    }

    public ngOnInit(): void {

        this.annee = new Date().getFullYear();
        let that = this;
        this._storage.get('user').then((val) => {
            that.uid = val.uid;
            that.urlConcours = '/' + that.uid + '/concours';
            that.myConcours = that.db.list(that.urlConcours).valueChanges();
            that.getNbChampionnats(that.annee);
        });

        // init form
        this.initForm();
    }

    private initForm(): void {
        this.concoursForm = {
            id: null,
            ville: '',
            club: '',
            date: '',
            formation: '',
            nbEquipes: '',
            nbPartiesGagnees: '',
            nbPartiesPerdues: '',
            resultat: '',
            poste: '',
            points: 0,
            repechage: '',
            partenaires: []
        };
    }


    public addPartenaire(partenaire: string): void {
        this.concoursForm.partenaires.push(partenaire);
        console.log(this.concoursForm);
    }

    private getNbChampionnats(year: number): void {
        this.nbConcours = 0;
        this.myConcours.forEach(element => {
            element.forEach(championnat => {
                if (championnat.annee === year) {
                    this.nbConcours++;
                }
            });
        });
    }

    public onChangeYear($event): void {
        this.getNbChampionnats($event);
    }

    public save(): void {

        if (this.concoursForm.id && this.concoursForm.id !== null && this.concoursForm.id !== '') {
            this.db.list(this.urlConcours).update(this.concoursForm.id, this.concoursForm);
        } else {
            this.db.list(this.urlConcours).push(this.concoursForm);
        }

        this.displayFormConcours = false;
    }

    public editConcours(concours: any): void {
        console.log(concours);
        /*this.qualifForm.controls['id'].setValue(concours.$key);
         this.qualifForm.controls['ville'].setValue(concours.ville);
         this.qualifForm.controls['club'].setValue(concours.name);
         this.qualifForm.controls['date'].setValue(concours.date);
         this.qualifForm.controls['formation'].setValue(concours.formation);
         this.qualifForm.controls['nbEquipes'].setValue(concours.nbEquipes);
         this.qualifForm.controls['nbPartiesGagnees'].setValue(concours.nbPartiesGagnees);
         this.qualifForm.controls['nbPartiesPerdues'].setValue(concours.nbPartiesPerdues);
         this.qualifForm.controls['resultat'].setValue(concours.resultat);
         this.qualifForm.controls['repechage'].setValue(concours.repechage);
         this.qualifForm.controls['poste'].setValue(concours.poste);
         this.qualifForm.controls['points'].setValue(concours.points);

         this.displayFormQualif = true;*/
    }

    public generateFormation(formation: string): void {
        console.log(formation);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PetanquePage');
    }

    public saveConcours(): void {
        const id: string = this.concoursForm.id;
        this.concoursForm.partenaires = [];
        this.concoursForm.partenaires.push(this.partenaire1);
        if(this.partenaire2 && this.partenaire2 !== ''){
            this.concoursForm.partenaires.push(this.partenaire2);
        }

        let concoursToSave = {
            name: this.concoursForm.club ? this.concoursForm.club : '',
            ville: this.concoursForm.ville ? this.concoursForm.ville : '',
            date: this.concoursForm.date ? this.concoursForm.date : '',
            annee: this.concoursForm.date ? new Date(this.concoursForm.date).getFullYear() : new Date().getFullYear(),
            formation: this.concoursForm.formation ? this.concoursForm.formation : '',
            nbEquipes: this.concoursForm.nbEquipes ? this.concoursForm.nbEquipes : '',
            nbPartiesGagnees: this.concoursForm.nbPartiesGagnees ? this.concoursForm.nbPartiesGagnees : '',
            nbPartiesPerdues: this.concoursForm.nbPartiesPerdues ? this.concoursForm.nbPartiesPerdues : '',
            resultat: this.concoursForm.resultat ? this.concoursForm.resultat : '',
            repechage: this.concoursForm.repechage ? this.concoursForm.repechage : false,
            poste: this.concoursForm.poste ? this.concoursForm.poste : '',
            points: this.concoursForm.points ? this.concoursForm.points : 0,
            partenaires: this.concoursForm.partenaires
        };

        concoursToSave.poste = concoursToSave.poste.toLowerCase();

        console.log(concoursToSave);

        /*if (id && id !== null && id !== '') {
            this.db.list(this.urlConcours).update(id, concoursToSave);
        } else {
            this.db.list(this.urlConcours).push(concoursToSave);
        }

        this.displayFormConcours = false;*/
    }

}
