import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PetanquePage} from './petanque';
import {QualifComponent} from './qualif/qualif';
import {EditConcoursComponent} from './concours/editConcours';
import {ConcoursComponent} from './concours/concours';
import {YearPipe} from '../../pipes/year/year';

import {FormationComponent} from '../../components/formation/formation';
@NgModule({
    declarations: [
        PetanquePage,
        YearPipe,
        FormationComponent,
        QualifComponent,
        EditConcoursComponent,
        ConcoursComponent
    ],
    imports: [
        IonicPageModule.forChild(PetanquePage),
    ],
})
export class PetanquePageModule {
}
