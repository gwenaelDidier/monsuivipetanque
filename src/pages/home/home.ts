import { Component } from '@angular/core';
import { NavController, Platform, ActionSheetController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { AuthProvider } from '../../providers/auth/auth';

import { ISport } from '../../interfaces/isport.interface';
import { PetanquePage } from '../petanque/petanque';
import { AccountPage } from '../account/account';
import { CalendrierPage } from '../calendrier/calendrier';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public displayName;
  public connected: boolean;
  public userImageUrl: string;
  public urlProfil: string;
  public mySports: Array<ISport> = [];
  public myProfilDB: AngularFireList<any[]>;
  public myProfil: any = {};
  public email: string = '';
  public password: string = '';

  constructor(private nav: NavController,
    private afAuth: AngularFireAuth,
    public _authProvider: AuthProvider,
    private _storage: Storage,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    private db: AngularFireDatabase) {

    this.init();
  }

  private init(): void {
    this.checkUserIsConnected();

    this.afAuth.authState.subscribe(user => {
      if (!user) {
        this.displayName = null;
        return;
      }

      this.userImageUrl = user.photoURL;
      this._authProvider.setUser(user);
      this._authProvider.setName(this.displayName);
      this._storage.set('user', user);
      this._authProvider.setConnected();

    });
  }
  public getMySports(): void {

    // return fake reponse
    let petanque: ISport = {
      name: 'Pétanque',
      img: 'assets/img/petanque.jpg',
      url: PetanquePage
    };
    this.mySports.push(petanque);
  }

  public accessSport(sport: ISport): void {
    this.nav.setRoot(sport.url);
  }

  public accessPetanquePage() {
    this.nav.setRoot(PetanquePage);
  }

  public accessCalendrierPage() {
    this.nav.setRoot(CalendrierPage);
  }

  public accessAccountPage() {
    this.nav.setRoot(AccountPage);
  }

  public openConnectMenu(): void {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Menu',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Déconnexion',
          icon: 'contact',
          handler: () => {
            console.log('deconnexion');
            this.signOut();
          }
        }
      ]
    });
    actionSheet.present();
  }

  login(email: string, password: string) {
    this.afAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked!');
        this.nav.setRoot(HomePage)
      })
      .catch(err => {
        console.log('Something went wrong:', err.message);
      });
  }
  signInWithFacebook() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => this.nav.setRoot(HomePage));
  }

  signInWithGoogle() {
    console.log('sign in with google');
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        console.log(res);
        this.nav.setRoot(HomePage);});
  }

  signOut() {
    this.afAuth.auth.signOut();
    this.connected = false;
    this.displayName = null;
    this._authProvider.setUser(null);
    this._storage.clear();
  }

  private getProfil(): void {
    this.myProfilDB = this.db.list(this.urlProfil);
    this.myProfilDB.valueChanges().subscribe((profil) => {
      this.myProfil = profil[0];
      this.displayName = this.myProfil.pseudo;
      if(profil.length === 0){
        this.accessAccountPage();
      }
    });
  }

  private checkUserIsConnected(): void {

    let self = this;
    this.connected = false;
    this._authProvider.setUser(null);
    this._storage.get('user').then((val) => {
      if (val && val != null) {
        self._authProvider.setUser(val);
        self._authProvider.setConnected();
        self.connected = true;
        self.urlProfil = '/' + val.uid + '/profil';
        self.getProfil();
        //self.getMySports();
      }
    });

  }


}
