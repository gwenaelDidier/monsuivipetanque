import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from 'angularfire2/database';

import { HomePage } from '../home/home';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage implements OnInit {

  public uid:string;
  public urlProfil: string;
  public hasNoProfil:boolean = true;
  public user: any;
  public accountForm: FormGroup;

  constructor(private nav: NavController,
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public _authProvider: AuthProvider,
    private _storage: Storage,
    private db: AngularFireDatabase) {
  }

  public ngOnInit(): void {

    this.accountForm = this.formBuilder.group({
      pseudo: ['', Validators.required],
      email: ['', Validators.required],
      ville: ['', Validators.required],
      club: ['', Validators.required]
    });

    //let user = this._authService.getUser();
    this._storage.get('user').then(user => {
      this.uid = user.uid;
      this.user = user;
      this.setAccountValue(user);
      this.urlProfil = '/' + this.uid + '/profil';
      let myProfil = this.db.list(this.urlProfil);
      myProfil.valueChanges().subscribe((profil) => {
        if (profil.length > 0) {
          this.hasNoProfil = false;
          this.setAccountValue(profil[0]);
        }
      });
    })
  }

  private setAccountValue(user: any): void {
    this.accountForm.controls['email'].setValue(user.email);
    const pseudo = user.pseudo ? user.pseudo : '';
    this.accountForm.controls['pseudo'].setValue(pseudo);
    const ville = user.ville ? user.ville : '';
    this.accountForm.controls['ville'].setValue(ville);
    const club = user.club ? user.club : '';
    this.accountForm.controls['club'].setValue(club);
  }

  public saveAccount(): void {
    const account = {
      email: this.accountForm.controls['email'].value,
      pseudo: this.accountForm.controls['pseudo'].value,
      ville: this.accountForm.controls['ville'].value,
      club: this.accountForm.controls['club'].value
    };
    let myProfil = this.db.list(this.urlProfil);
    myProfil.snapshotChanges().subscribe((profil) => {
      if (profil.length == 0) {
        myProfil.push(account);
      }else{
        myProfil.update(profil[0].key, account);
      }
      this.nav.setRoot(HomePage)
    });
  }

  public annuler(): void {
      this.nav.setRoot(HomePage);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

}
