import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

/**
 * Generated class for the CalendrierPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-calendrier',
  templateUrl: 'calendrier.html',
})
export class CalendrierPage {

  public msg: string = '';
  public err: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private calendar: Calendar) {

    this.calendar.createCalendar('MyCalendar').then(
      (msg) => { this.msg = msg; console.log(msg); },
      (err) => { this.err = err; console.log(err); }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendrierPage');
  }

}
