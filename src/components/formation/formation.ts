import { Component, OnInit, Input } from '@angular/core';

/**
 * Generated class for the FormationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'formation',
  templateUrl: 'formation.html'
})
export class FormationComponent implements OnInit{

  @Input('formation')
  formation: string;

  public myFormation: string;

  constructor() {

  }

  ngOnInit(){
    switch (this.formation){

      case 'TAT':
        this.myFormation = 'teteatete';
        break;
      case 'TATF':
        this.myFormation = 'teteatete feminin';
        break;

      case 'D':
        this.myFormation = 'doublette';
        break;
      case 'DM':
        this.myFormation = 'doublette mixte';
        break;
      case 'DF':
        this.myFormation = 'doublette feminin';
        break;

      case 'T':
        this.myFormation = 'triplette';
        break;
      case 'TM':
        this.myFormation = 'triplette mixte';
        break;
      case 'TF':
        this.myFormation = 'triplette feminin';
        break;
    }
  }

}
