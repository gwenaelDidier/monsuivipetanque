import { NgModule } from '@angular/core';
import { FormationComponent } from './formation/formation';
@NgModule({
	declarations: [FormationComponent],
	imports: [],
	exports: [FormationComponent]
})
export class ComponentsModule {}
