import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { CalendrierPage } from '../pages/calendrier/calendrier';

import { PetanquePageModule } from '../pages/petanque/petanque.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { ConcoursService } from '../pages/petanque/concours/concours.service';

import { FormationComponent } from '../components/formation/formation';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';

export const firebaseConfig = {
  apiKey: "AIzaSyD98PVQVbLVSIFUPEmZwb0OEDMD20LncfQ",
  authDomain: "suivisport-4aada.firebaseapp.com",
  databaseURL: "https://suivisport-4aada.firebaseio.com",
  projectId: "suivisport-4aada",
  storageBucket: "suivisport-4aada.appspot.com",
  messagingSenderId: "744465379112"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AccountPage,
    CalendrierPage,
  ],
  imports: [
    BrowserModule,
    PetanquePageModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite', 'websql', 'indexeddb']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AccountPage,
    CalendrierPage,
    FormationComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider, ConcoursService,
    Calendar
  ]
})
export class AppModule {}
