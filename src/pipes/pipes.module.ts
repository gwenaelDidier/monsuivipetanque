import { NgModule } from '@angular/core';
import { YearPipe } from './year/year';
@NgModule({
	declarations: [YearPipe],
	imports: [],
	exports: [YearPipe]
})
export class PipesModule {}
