import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the YearPipe pipe.
 *
 */
@Pipe({
  name: 'year',
})
export class YearPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: Array<any>, myYear: number) {

    if(value && value !== null && value.length > 0){
      return value.filter(_val => {
        return _val.annee == myYear;
      });
    } else {
      return [];
    }
  }
}
